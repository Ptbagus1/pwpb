<?php
class Controller{
    // ....
    public function view($view, $data = []) {
        // require_once "../app/view/{$view}.php";
        require_once "../app/view/" . $view . ".php";
    }
    public function model($modelName)
    {
        require_once "../app/model/" . $modelName . ".php";
        return new $modelName();
    }

}

