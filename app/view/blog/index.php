<div class="container mt-5">
    <div class="row">
        <div class="col-6">
        <h3>Blog</h3> 
        <ul class="list-group">
            <?php foreach($data['blog'] as $blog) :?>
            <li class="list-group-item list-group-item d-flex
            justify-content-between align-item-center">
                <?php echo $blog['judul']?>
                <?php echo $blog['isi']; ?>
            </li>
            <?php endforeach ;?>
        </ul> 
        </div>
    </div>
</div>