<?php

class User_model 
{
    private ?Database $db;
    public string $table = "user";

    public function __construct()
    {
        $this->db = new Database(); 
    }

    public function tambah_user(array $data)
    {
        $sql = "INSERT INTO user (username, email, password) VALUES (:username, :email, :password)";
     
        $this->db->query($sql);

        $this->db->bind(":username", $data['username']);
        $this->db->bind(":password", $data['password']);
        $this->db->bind(":email", $data['email']);

    
        return $this->db->resultSingle();
    }

    public function coba_login(array $data)
    {
        $sql = "SELECT * FROM user WHERE username = :username AND password = :password";
     
        $this->db->query($sql);
        $this->db->bind(":username", $data['username']);
        $this->db->bind(":password", $data['password']);

        var_dump($this->db->execute());
        return $this->db->resultSingle();
        
    }

}