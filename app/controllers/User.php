<?php
 class User extends Controller {

    private User_model $userModel;

    public function __construct()
    {
      $this->userModel = $this->model("User_model");
    }

    public function index() 
    {  
      $data["judul"] = "User";
      $this->view("templates/header", $data);
      $this->view("user/index");
      $this->view("templates/footer");
    }

    public function profile($nama = "Linux", $pekerjaan = "Devs") 
    {
        $data["nama"] = $nama;
        $data["pekerjaan"] = $pekerjaan;
        $this->view("user/profile", $data);
        $data["judul"] = "User";
        $this->view("templates/header", $data);
        $this->view("user/profile", $data);
        $this->view("templates/footer");
    }

    public function login() 
    {
      $data['title'] = "login";
      $this->view("user/login", $data);

    }
    public function register() 
    {
      $data['title'] = "register cok";
      $this->view("user/register", $data);

    }

    public function handle_login(){
      $username = $_POST['username'];
      $password = Hash::make($_POST['password']);

      $user = $this->userModel->coba_login([
        "username" => $username,
        "password" => $password
      ]);

      if($user){
        $_SESSION['user_login'] = $user;
        return redirect("HOME");
      }
      else {
        return redirect("user/login", ['fail' => "username atau password salah"]);
      }
    }

    public function simpan_register()
    {

      $username = $_POST['username'];
      $password = Hash::make($_POST['password']);
      $email = $_POST['email'];

      $this->userModel->tambah_user([
        "username" => $username,
        'password' => $password,
        'email' => $email
      ]);

      return redirect("user/login");
    }
    public function logout()
    {
      session_unset();
      session_destroy();
      return redirect('user/login');
    }
  } 
