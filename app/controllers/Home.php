<?php

class Home extends Controller {
    
    public function __construct()
    {
        if(! isset($_SESSION['user_login']))
        {
            //belum login maka
            return redirect("user/login");
        }
    }

    public function index()
    {
        $data['judul'] = "Home";
        // $data['nama'] = "bagus";
    
        $this->view("templates/header", $data);
        $this->view("home/index");
        $this->view("templates/footer");
  
    }

}