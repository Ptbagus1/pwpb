<?php

//redirect( 'admin/' )


function url(string $url)
{

    return BASEURL . '/'.$url;
}

function redirect(string $path){
    $path = url($path);
    header("Location: $path");
    die;
}

function close_all_session_except(string ...$protected_sessions)
{
    $session_keys = array_keys($_SESSION);

    foreach($session_keys as $session){
        if(! in_array($session, $protected_sessions )){
            unset($_SESSION[$session]);
        }
    }
}